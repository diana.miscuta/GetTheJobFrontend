import { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import MainNavigation from "./components/landing page components/navigation/MainNavigation";
import { GetTheJobContext } from "./context/GetTheJobContext";
import CalendarPage from "./pages/CalendarPage";
import { ChecklistPage } from "./pages/ChecklistPage";
import Dashboard from "./pages/Dashboard";
import LandingPage from "./pages/LandingPage";
import { Login } from "./pages/Login";
import ResumeBuilderPage from "./pages/ResumeBuilderPage";
import { Signup } from "./pages/Signup";
import Resume from "./pages/ResumePage";
import HomeNavigation from "./components/landing page components/navigation/HomeNavigation";
import ResumePreviewPage from "./pages/ResumePreviewPage";
import ForumPage from "./pages/ForumPage";
import AdminDashboard from "./pages/AdminDashboard";
import UserInterface from "./interfaces/UserInterface";

function App() {
  const [id, setId] = useState("");
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const [resumeId, setResumeId] = useState("");

  const [sectionId, setSectionId] = useState("");

  useEffect(() => {
    (async () => {
      const response = await fetch("http://localhost:5000/api/Users/user", {
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      });
      const content = await response.json();
      setId(content.userId);
      setEmail(content.userEmail);
      setFirstName(content.userFirstName);
      setLastName(content.userLastName);
    })();
  }, [id]);

  const context = {
    userId: id,
    setId: setId,
    userFirstName: firstName,
    setFirstName: setFirstName,
    userLastName: lastName,
    setLastName: setLastName,
    userEmail: email,
    setUserEmail: setEmail,
    resumeId: resumeId,
    setResumeId: setResumeId,
    sectionId: sectionId,
    setSectionId: setSectionId,
  };

  return (
    <GetTheJobContext.Provider value={context}>
      <Router>
        {!id ? <HomeNavigation /> : <MainNavigation />}
        <main>
          <Routes>
            <Route path="/" element={id ? <Dashboard /> : <LandingPage />} />
            <Route path="/login" element={<Login />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/checklists" element={<ChecklistPage />} />
            <Route path="/calendar" element={<CalendarPage />} />
            <Route path="/resumes" element={<Resume />} />
            <Route path="/builder" element={<ResumeBuilderPage />} />
            <Route path="/preview" element={<ResumePreviewPage />} />
            <Route path="/forum" element={<ForumPage />} />
            <Route path="/admin" element={<AdminDashboard />} />
          </Routes>
        </main>
      </Router>
    </GetTheJobContext.Provider>
  );
}

export default App;
