import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import classes from "./AddPost.module.css";
import { useContext, useState } from "react";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { ToastContainer, toast } from "react-toastify";

export const AddPost = (props: { show: boolean; handleClose: () => void }) => {
  const { sectionId, userId } = useContext(GetTheJobContext);
  const [postText, setPostText] = useState("");
  const submitHandler = async () => {
    if (postText) {
      await fetch("http://localhost:5000/api/Posts", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          sectionId: sectionId,
          userId: userId,
          postedDate: new Date(),
          postText: postText,
          status: false,
        }),
      });
      props.handleClose();
    } else {
      toast.error("No text added to post!", {
        position: "bottom-left",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const htmlAttributes = { rows: "4" };

  if (!props.show) {
    return null;
  }

  return (
    <div className={classes.container}>
      <div className={classes.addPost}>
        <TextBoxComponent
          id="default"
          multiline={true}
          floatLabelType="Auto"
          placeholder="New post"
          htmlAttributes={htmlAttributes}
          onChange={(e: any) => setPostText(e.target.value)}
        />
        <div className={classes.footer}>
          <TooltipComponent
            className="tooltip-box"
            content="New posts must be approved by an admin before being published on the forum."
            openDelay={500}
          >
            <AiOutlineExclamationCircle className={classes.tooltip} />
          </TooltipComponent>
          <div className={classes.actions}>
            <button
              type="button"
              onClick={props.handleClose}
              className={classes.cancel}
            >
              Cancel
            </button>
            <button
              type="button"
              onClick={submitHandler}
              className={classes.post}
            >
              Post
            </button>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddPost;
