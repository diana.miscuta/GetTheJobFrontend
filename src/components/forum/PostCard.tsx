import classes from "./PostCard.module.css";

export const PostCard = (props: {
  postId: string;
  userFirstName: string;
  userLastName: string;
  postedDate: string;
  postText: string;
}) => {
  const options: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "long",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
  };
  const postDate = new Date(props.postedDate);
  const postDateString = postDate.toLocaleString("en-GB", options);

  return (
    <div key={props.postId} className={classes.container}>
      <div className={classes.header}>
        <div className={classes.user}>
          {props.userFirstName} {props.userLastName}
        </div>
        <div className={classes.date}>{postDateString}</div>
      </div>
      <div className={classes.text} style={{ whiteSpace: "pre-line" }}>
        {props.postText}
      </div>
    </div>
  );
};

export default PostCard;
