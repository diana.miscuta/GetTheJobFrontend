import classes from "./PostCard.module.css";

export const SectionCard = (props: {
  sectionId: string;
  userFirstName: string;
  userLastName: string;
  sectionName: string;
}) => {
  return (
    <div key={props.sectionId} className={classes.container}>
      <div className={classes.text}>{props.sectionName} section requested</div>
    </div>
  );
};

export default SectionCard;
