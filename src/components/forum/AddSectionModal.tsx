import { useContext, useState } from "react";
import classes from "./AddSectionModal.module.css";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { ToastContainer, toast } from "react-toastify";

export const AddSectionModal = (props: {
  show: boolean;
  handleClose: () => void;
}) => {
  const { userId } = useContext(GetTheJobContext);
  const [name, setName] = useState("");

  const submitHandler = async () => {
    console.log("user id aici ", userId);
    if (name) {
      await fetch("http://localhost:5000/api/Sections", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          sectionName: name,
          userId: userId,
          status: false,
        }),
      });
      props.handleClose();
    } else {
      toast.error("No section name added!", {
        position: "bottom-left",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  if (!props.show) {
    return null;
  }

  return (
    <div className={classes.container}>
      <div className={classes.popup}>
        <div className={classes.input}>
          <TextBoxComponent
            placeholder="Section name"
            cssClass="e-outline"
            id="name"
            onChange={(e: any) => setName(e.target.value)}
          />
        </div>
        <div className={classes.footer}>
          <TooltipComponent
            className="tooltip-box"
            content="New sections must be approved by an admin before being published on the forum."
            openDelay={500}
          >
            <AiOutlineExclamationCircle className={classes.tooltip} />
          </TooltipComponent>
          <div className={classes.actions}>
            <button
              type="button"
              className={classes.cancel}
              onClick={props.handleClose}
            >
              Cancel
            </button>
            <button
              type="button"
              className={classes.done}
              onClick={submitHandler}
            >
              Done
            </button>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddSectionModal;
