import { useContext, useState } from "react";
import classes from "./AddChecklist.module.css";
import { GetTheJobContext } from "../../../context/GetTheJobContext";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { ToastContainer, toast } from "react-toastify";

export const AddChecklist = (props: {
  show: boolean;
  handleClose: () => void;
}) => {
  const { userId } = useContext(GetTheJobContext);
  const [checklistName, setChecklistName] = useState("");

  const submitAddChecklistHandler = async () => {
    if (checklistName) {
      await fetch("http://localhost:5000/api/Checklists", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          checklistName: checklistName,
          userId: userId,
        }),
      });
      props.handleClose();
    } else {
      toast.error("No checklist name has been added!", {
        position: "bottom-left",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  if (!props.show) {
    return null;
  }

  return (
    <div className={classes.container}>
      <div className={classes.popup}>
        <div className={classes.input}>
          <TextBoxComponent
            placeholder="Checklist name"
            cssClass="e-outline"
            id="name"
            onChange={(e: any) => setChecklistName(e.target.value)}
          />
        </div>
        <div className={classes.actions}>
          <button
            type="button"
            className={classes.cancel}
            onClick={props.handleClose}
          >
            Cancel
          </button>
          <button
            type="button"
            className={classes.done}
            onClick={submitAddChecklistHandler}
          >
            Done
          </button>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddChecklist;
