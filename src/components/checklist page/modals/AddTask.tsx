import { useState } from "react";
import classes from "./AddTask.module.css";
import { ToastContainer, toast } from "react-toastify";
import { BsCheck2, BsXCircle } from "react-icons/bs";

export const AddTask = (props: {
  checklistId: string;
  show: boolean;
  handleClose: () => void;
}) => {
  const [text, setText] = useState("");

  const submitHandler = async () => {
    if (text) {
      await fetch("http://localhost:5000/api/ChecklistItems", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          checklistId: props.checklistId,
          text: text,
          status: false,
        }),
      });
      props.handleClose();
    } else {
      toast.error("No task added!", {
        position: "bottom-left",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  if (!props.show) {
    return null;
  }

  return (
    <div className={classes.add}>
      <div className="col-xs-6 col-sm-6 col-lg-6 col-md-6 small-textbox">
        <div className="e-input-group e-small">
          <input
            className="e-input"
            type="text"
            placeholder="Add a task"
            onChange={(e) => setText(e.target.value)}
            style={{ minWidth: "15rem" }}
          />
        </div>
      </div>
      <div className={classes.actions}>
        <button type="button" className={classes.done} onClick={submitHandler}>
          <BsCheck2 />
        </button>
        <button
          type="button"
          className={classes.cancel}
          onClick={props.handleClose}
        >
          <BsXCircle />
        </button>
        <ToastContainer />
      </div>
    </div>
  );
};

export default AddTask;
