import { useEffect, useState } from "react";
import { MdAdd, MdDeleteOutline } from "react-icons/md";
import ChecklistItem from "../checklist item/ChecklistItem";
import AddTask from "../modals/AddTask";
import classes from "./Checklist.module.css";
import ChecklistItemInterface from "../../../interfaces/ChecklistItemInterface";

export const Checklist = (props: { checklistId: string; title: string }) => {
  const [checklistItems, setChecklistItems] = useState<
    ChecklistItemInterface[]
  >([]);

  useEffect(() => {
    fetch(
      `http://localhost:5000/api/ChecklistItems/checklist/${props.checklistId}`
    )
      .then((response) => response.json())
      .then((data) => {
        setChecklistItems(data);
      })
      .catch((error) => {
        console.log("error fetching checklist items: ", error);
      });
  }, [checklistItems]);

  const [showModal, setShowModal] = useState(false);
  const show = () => {
    setShowModal(true);
  };
  const hide = () => {
    setShowModal(false);
  };

  const deleteHandler = () => {
    fetch(`http://localhost:5000/api/Checklists/${props.checklistId}`, {
      method: "DELETE",
    }).catch((error) => console.log(error));
  };

  return (
    <div key={props.checklistId} className={classes.container}>
      <div className={classes.header}>
        <p>{props.title}</p>
        <div>
          <button type="button" className={classes.actions} onClick={show}>
            <MdAdd />
          </button>
          <button
            className={classes.actions}
            type="button"
            onClick={deleteHandler}
          >
            <MdDeleteOutline />
          </button>
        </div>
      </div>
      <div className={classes.tasks}>
        <AddTask
          checklistId={props.checklistId}
          show={showModal}
          handleClose={hide}
        />
        {checklistItems.map((item) => (
          <ChecklistItem
            itemId={item.itemId}
            text={item.text}
            status={item.status}
          />
        ))}
      </div>
    </div>
  );
};

export default Checklist;
