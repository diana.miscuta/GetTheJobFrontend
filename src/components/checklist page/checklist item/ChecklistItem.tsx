import { useState } from "react";
import { MdDeleteOutline, MdOutlineEdit } from "react-icons/md";
import classes from "./ChecklistItem.module.css";
import { BsCheck2, BsXCircle } from "react-icons/bs";

export const ChecklistItem = (props: {
  itemId: string;
  text: string;
  status: boolean;
}) => {
  const checkHandler = async () => {
    await fetch(`http://localhost:5000/api/ChecklistItems/${props.itemId}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
      body: JSON.stringify({
        text: props.text,
        status: !props.status,
      }),
    });
  };

  const [edit, setEdit] = useState(false);
  const [text, setText] = useState(props.text);

  const setToEdit = () => {
    setEdit(true);
  };

  const cancelHandler = () => {
    setEdit(false);
  };

  const updateHandler = async () => {
    await fetch(`http://localhost:5000/api/ChecklistItems/${props.itemId}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
      body: JSON.stringify({
        text: text,
        status: props.status,
      }),
    });
    setEdit(false);
  };

  const deleteHandler = () => {
    fetch(`http://localhost:5000/api/ChecklistItems/${props.itemId}`, {
      method: "DELETE",
    }).catch((error) => console.log(error));
  };

  const [show, setShow] = useState(false);
  const showButton = !show ? "hidden" : "visible";

  return (
    <div key={props.itemId} className={classes.container}>
      {!edit && (
        <div className={classes.task}>
          <div className={classes.content}>
            <input
              type="checkbox"
              checked={props.status}
              onChange={checkHandler}
            />
            <span className={classes.checkmark}></span>
            <p onClick={() => setShow(!show)}>{props.text}</p>
          </div>
          <div className={classes.actions} style={{ visibility: showButton }}>
            <button type="button" onClick={setToEdit}>
              <MdOutlineEdit />
            </button>
            <button type="button" onClick={deleteHandler}>
              <MdDeleteOutline />
            </button>
          </div>
        </div>
      )}
      {edit && (
        <div className={classes.task}>
          <div className={classes.content} onClick={() => setShow(!show)}>
            <input
              type="text"
              value={text}
              checked={props.status}
              onChange={(e: any) => setText(e.target.value)}
            />
          </div>
          <div className={classes.actions}>
            <button type="button" onClick={updateHandler}>
              <BsCheck2 />
            </button>
            <button type="button" onClick={cancelHandler}>
              <BsXCircle />
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default ChecklistItem;
