import { useState } from "react";
import { Link, Navigate } from "react-router-dom";
import classes from "./SignupForm.module.css";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { ToastContainer, toast } from "react-toastify";

export const SignupForm = () => {
  const [userEmail, setEmail] = useState("");
  const [userPassword, setPassword] = useState("");
  const [userFirstName, setFirstname] = useState("");
  const [userLastName, setLastname] = useState("");
  const [redirect, setRedirect] = useState(false);

  const submitHandler = async (event: { preventDefault: () => void }) => {
    event.preventDefault();
    const formValidation =
      userFirstName !== "" &&
      userLastName !== "" &&
      userPassword !== "" &&
      userEmail !== "";
    if (formValidation) {
      await fetch("http://localhost:5000/api/Users/Register", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          userEmail: userEmail,
          userPassword: userPassword,
          userFirstName: userFirstName,
          userLastName: userLastName,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.message !== "Success") {
            toast.error(data.message, {
              position: "bottom-left",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "light",
            });
          } else {
            setRedirect(true);
          }
        });
    } else {
      toast.error("The form is not complete.", {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  let htmlAttributes = { type: "email", require: "true" };

  if (redirect) return <Navigate to="/login" />;

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <h1>SIGN UP</h1>
        <p>Welcome! Please create your account.</p>
      </div>
      <form className={classes.form}>
        <div className={classes.input}>
          <TextBoxComponent
            placeholder="First name"
            cssClass="e-outline"
            id="firstName"
            onChange={(e: any) => setFirstname(e.target.value)}
          />
        </div>
        <div className={classes.input}>
          <TextBoxComponent
            placeholder="Last name"
            cssClass="e-outline"
            id="lastName"
            onChange={(e: any) => setLastname(e.target.value)}
          />
        </div>
        <div className={classes.input}>
          <TextBoxComponent
            placeholder="Email"
            cssClass="e-outline"
            id="email"
            type="email"
            htmlAttributes={htmlAttributes}
            onChange={(e: any) => setEmail(e.target.value)}
          />
        </div>
        <div className={classes.input}>
          <TextBoxComponent
            type="password"
            placeholder="Password"
            cssClass="e-outline"
            id="password"
            onChange={(e: any) => setPassword(e.target.value)}
          />
        </div>
        <div className={classes.actions}>
          <button
            type="submit"
            onClick={submitHandler}
            className={classes.submit}
          >
            SIGN UP
          </button>
        </div>
      </form>
      <div className={classes.redirect}>
        <p>Already got an account?</p>
        <Link to="/login" className={classes.linklogin}>
          <div className={classes.login}>
            <p>SIGN IN</p>
          </div>
        </Link>
      </div>
      <ToastContainer />
    </div>
  );
};

export default SignupForm;
