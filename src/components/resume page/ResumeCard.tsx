import { RiFile3Line } from "react-icons/ri";
import classes from "./ResumeCard.module.css";
import {
  MdDeleteOutline,
  MdOutlineModeEditOutline,
  MdOutlinePreview,
} from "react-icons/md";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { GetTheJobContext } from "../../context/GetTheJobContext";

export const ResumeCard = (props: {
  resumeId: string;
  resumeName: string;
  creationDate: string;
}) => {
  const date = new Date(props.creationDate);
  const options: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  };

  const navigate = useNavigate();

  const { setResumeId } = useContext(GetTheJobContext);

  const handleEdit = () => {
    setResumeId(props.resumeId);
    navigate("/builder");
  };

  const handlePreview = () => {
    setResumeId(props.resumeId);
    navigate("/preview");
  };

  const handleDelete = () => {
    fetch(`http://localhost:5000/api/Resumes/${props.resumeId}`, {
      method: "DELETE",
    }).catch((error) => console.log(error));
  };

  return (
    <div key={props.resumeId} className={classes.container}>
      <div className={classes.left}>
        <RiFile3Line className={classes.icon} />
        <div className={classes.info}>
          <div className={classes.title}>{props.resumeName}</div>
          <div className={classes.createdAt}>
            created at {date.toLocaleString("ro-RO", options)}
          </div>
        </div>
      </div>
      <div className={classes.actions}>
        <TooltipComponent
          className="tooltip-box"
          content="Edit"
          openDelay={500}
        >
          <button type="button" onClick={handleEdit}>
            <MdOutlineModeEditOutline />
          </button>
        </TooltipComponent>
        <TooltipComponent
          className="tooltip-box"
          content="Preview"
          openDelay={500}
        >
          <button type="button" onClick={handlePreview}>
            <MdOutlinePreview />
          </button>
        </TooltipComponent>
        <TooltipComponent
          className="tooltip-box"
          content="Delete"
          openDelay={500}
        >
          <button type="button" onClick={handleDelete}>
            <MdDeleteOutline />
          </button>
        </TooltipComponent>
      </div>
    </div>
  );
};

export default ResumeCard;
