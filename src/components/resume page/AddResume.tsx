import { useContext, useState } from "react";
import classes from "./AddResume.module.css";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { ToastContainer, toast } from "react-toastify";

export const AddResume = (props: {
  show: boolean;
  handleClose: () => void;
}) => {
  const { userId } = useContext(GetTheJobContext);
  const [title, setTitle] = useState("");

  const submitHandler = async () => {
    if (title) {
      await fetch("http://localhost:5000/api/Resumes", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          resumeName: title,
          userId: userId,
          creationDate: new Date(),
        }),
      });
      props.handleClose();
    } else {
      toast.error("No resume name added!", {
        position: "bottom-left",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  if (!props.show) {
    return null;
  }

  return (
    <div className={classes.container}>
      <div className={classes.popup}>
        <div className={classes.input}>
          <TextBoxComponent
            placeholder="Resume name"
            cssClass="e-outline"
            id="name"
            onChange={(e: any) => setTitle(e.target.value)}
          />
        </div>
        <div className={classes.actions}>
          <button
            type="button"
            className={classes.cancel}
            onClick={props.handleClose}
          >
            Cancel
          </button>
          <button
            type="button"
            className={classes.done}
            onClick={submitHandler}
          >
            Done
          </button>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddResume;
