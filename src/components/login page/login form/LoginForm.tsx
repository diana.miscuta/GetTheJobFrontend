import { useContext, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import { GetTheJobContext } from "../../../context/GetTheJobContext";
import classes from "./LoginForm.module.css";
import { ToastContainer, toast } from "react-toastify";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";

export const LoginForm = () => {
  const [userEmail, setEmail] = useState("");
  const [userPassword, setPassword] = useState("");
  const [redirect, setRedirect] = useState(false);
  const { setId, userFirstName } = useContext(GetTheJobContext);
  if (redirect && userFirstName) return <Navigate to="/" />;
  const submitHandler = async (event: { preventDefault: () => void }) => {
    event.preventDefault();
    const formValidation = userPassword !== "" && userEmail !== "";
    if (formValidation) {
      await fetch("http://localhost:5000/api/Users/Login", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
        body: JSON.stringify({
          userEmail: userEmail,
          userPassword: userPassword,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.message !== "Success") {
            toast.error(data.message, {
              position: "bottom-left",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "light",
            });
          }
        });
      setId("");
      setRedirect(true);
    } else {
      toast.error("The form is not complete.", {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <h1>SIGN IN</h1>
        <p>Welcome back! Please sign into your account.</p>
      </div>
      <form className={classes.form} onSubmit={submitHandler}>
        <div className={classes.input}>
          <TextBoxComponent
            placeholder="Email"
            cssClass="e-outline"
            id="email"
            onChange={(e: any) => setEmail(e.target.value)}
          />
        </div>
        <div className={classes.input}>
          <TextBoxComponent
            type="password"
            placeholder="Password"
            cssClass="e-outline"
            id="password"
            onChange={(e: any) => setPassword(e.target.value)}
          />
        </div>
        <div className={classes.actions}>
          <button type="submit" className={classes.submit}>
            SIGN IN
          </button>
        </div>
      </form>
      <div className={classes.redirect}>
        <p>You don't have an account?</p>
        <Link to="/signup" className={classes.linksignup}>
          <div className={classes.signup}>
            <p>REGISTER</p>
          </div>
        </Link>
      </div>
      <ToastContainer />
    </div>
  );
};

export default LoginForm;
