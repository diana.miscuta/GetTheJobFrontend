import { useState, useContext, useEffect } from "react";
import classes from "./Goals.module.css";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { AiOutlineExclamationCircle } from "react-icons/ai";

export const Goals = () => {
  const [editGoals, setEditGoals] = useState(false);
  const { userId } = useContext(GetTheJobContext);
  const [goal1, setGoal1] = useState("");
  const [goal2, setGoal2] = useState("");
  const [goal3, setGoal3] = useState("");

  useEffect(() => {
    fetch(`http://localhost:5000/api/Users/${userId}`)
      .then((response) => response.json())
      .then((data) => {
        setGoal1(data.userGoal1);
        setGoal2(data.userGoal2);
        setGoal3(data.userGoal3);
      })
      .catch(() => {
        console.log("error fetching user goals");
      });
  }, [editGoals]);

  const editGoalsHandler = async () => {
    if (editGoals) {
      await fetch(`http://localhost:5000/api/Users/${userId}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          userGoal1: goal1,
          userGoal2: goal2,
          userGoal3: goal3,
        }),
      });
    }
    setEditGoals(!editGoals);
  };

  const htmlAttributes = { maxlength: "300" };

  return (
    <div className={classes.container}>
      <div className={classes.list}>
        <div className={classes.goal}>
          <h3>1.</h3>
          {editGoals ? (
            <TextBoxComponent
              placeholder="Add your first goal"
              cssClass="e-outline e-small"
              id="goal1"
              showClearButton={true}
              htmlAttributes={htmlAttributes}
              value={goal1}
              onChange={(e: any) => setGoal1(e.target.value)}
            />
          ) : goal1 != "" ? (
            goal1
          ) : (
            "No goal set"
          )}
        </div>
        <div className={classes.goal}>
          <h3>2.</h3>
          {editGoals ? (
            <TextBoxComponent
              placeholder="Add your second goal"
              cssClass="e-outline e-small"
              id="goal2"
              showClearButton={true}
              htmlAttributes={htmlAttributes}
              value={goal2}
              onChange={(e: any) => setGoal2(e.target.value)}
            />
          ) : goal2 != "" ? (
            goal2
          ) : (
            "No goal set"
          )}
        </div>
        <div className={classes.goal}>
          <h3>3.</h3>
          {editGoals ? (
            <TextBoxComponent
              placeholder="Add your third goal"
              cssClass="e-outline e-small"
              id="goal3"
              showClearButton={true}
              htmlAttributes={htmlAttributes}
              value={goal3}
              onChange={(e: any) => setGoal3(e.target.value)}
            />
          ) : goal3 != "" ? (
            goal3
          ) : (
            "No goal set"
          )}
        </div>
      </div>
      <div className={classes.actions}>
        <TooltipComponent
          className="tooltip-box"
          content="Maximum length is 300 characters."
          openDelay={500}
        >
          <AiOutlineExclamationCircle className={classes.tooltip} />
        </TooltipComponent>
        <button type="button" onClick={editGoalsHandler}>
          {editGoals ? "Done" : "Edit goals"}
        </button>
      </div>
    </div>
  );
};
