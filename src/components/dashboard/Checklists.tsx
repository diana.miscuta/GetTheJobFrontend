import { useState, useContext, useEffect } from "react";
import ChecklistCard from "./ChecklistCard";
import DashboardChecklistInterface from "../../interfaces/DashboardChecklistInterface";
import { GetTheJobContext } from "../../context/GetTheJobContext";

export const Checklists = () => {
  const { userId } = useContext(GetTheJobContext);
  const [checklists, setChecklists] = useState<DashboardChecklistInterface[]>(
    []
  );

  useEffect(() => {
    fetch(`http://localhost:5000/api/Checklists/Dashboard/${userId}`)
      .then((response) => response.json())
      .then((data) => {
        setChecklists(data);
      })
      .catch(() => {
        console.log("error fetching dashboard checklists");
      });
  }, [userId, checklists]);

  return (
    <div>
      <div>
        {checklists.map((checklist) => (
          <ChecklistCard
            checklistId={checklist.checklistId}
            checklistName={checklist.checklistName}
            checklistStatus={checklist.checklistStatus}
          />
        ))}
      </div>
    </div>
  );
};

export default Checklists;
