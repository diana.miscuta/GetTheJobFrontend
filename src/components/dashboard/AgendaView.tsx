import { Internationalization } from "@syncfusion/ej2-base";
import {
  Agenda,
  EventRenderedArgs,
  Inject,
  ScheduleComponent,
  ViewDirective,
  ViewsDirective,
} from "@syncfusion/ej2-react-schedule";
import { useContext, useEffect, useState } from "react";
import { GetTheJobContext } from "../../context/GetTheJobContext";

const AgendaView = () => {
  const mockData = [
    {
      eventId: "s",
      eventName: "Test",
      eventStartDate: new Date(2023, 5, 4, 5, 30),
      eventStopDate: new Date(2023, 5, 4, 6, 30),
      eventLocation: "Lugoj",
      eventDetails: "Doamne feri ce e aici",
      eventColor: 1,
      userId: "1",
    },
  ];

  const fieldsData = {
    id: "eventId",
    subject: { name: "eventName" },
    startTime: { name: "eventStartDate" },
    endTime: { name: "eventStopDate" },
    location: { name: "eventLocation" },
    description: { name: "eventDetails" },
  };

  const [events, setEvents] = useState(mockData);
  const { userId } = useContext(GetTheJobContext);

  useEffect(() => {
    fetch(`http://localhost:5000/api/Events/${userId}`)
      .then((response) => response.json())
      .then((data) => {
        setEvents(data);
      })
      .catch(() => {
        console.log("error fetching events");
      });
  }, []);

  const onEventRendered = (args: EventRenderedArgs) => {
    if (args.element.firstElementChild instanceof HTMLElement) {
      const firstElem = args.element.firstElementChild;

      switch (args.data.eventColor) {
        case 3:
          (firstElem as HTMLElement).style.borderLeftColor = "#93ae55";
          break;
        case 2:
          (firstElem as HTMLElement).style.borderLeftColor = "#F2D096";
          break;
        case 1:
          (firstElem as HTMLElement).style.borderLeftColor = "#e54a50";
          break;
      }
    }
  };

  const instance: Internationalization = new Internationalization();
  const getTimeString = (value: Date) => {
    return instance.formatDate(value, { skeleton: "hm" });
  };

  const eventTemplate = (props: {
    eventName: string;
    eventStartDate: Date;
    eventStopDate: Date;
  }): JSX.Element => {
    return (
      <div className="template-wrap">
        <div className="subject">{props.eventName}</div>
        <div className="time">
          Time: {getTimeString(props.eventStartDate)} -{" "}
          {getTimeString(props.eventStopDate)}
        </div>
      </div>
    );
  };

  return (
    <ScheduleComponent
      width="100%"
      height="100%"
      agendaDaysCount={14}
      selectedDate={new Date()}
      eventRendered={onEventRendered.bind(this)}
      eventSettings={{ dataSource: events, fields: fieldsData }}
      readonly={true}
    >
      <ViewsDirective>
        <ViewDirective
          option="Agenda"
          eventTemplate={eventTemplate}
          allowVirtualScrolling={false}
        />
      </ViewsDirective>
      <Inject services={[Agenda]} />
    </ScheduleComponent>
  );
};

export default AgendaView;
