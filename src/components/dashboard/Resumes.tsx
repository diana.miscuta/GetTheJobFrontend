import { useState, useContext, useEffect } from "react";
import { ResumeInterface } from "../../interfaces/ResumeInterface";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import ResumeCard from "../resume page/ResumeCard";

export const Resumes = () => {
  const { userId } = useContext(GetTheJobContext);
  const [resumes, setResumes] = useState<ResumeInterface[]>([]);

  useEffect(() => {
    fetch(`http://localhost:5000/api/Resumes/User/${userId}`)
      .then((response) => response.json())
      .then((data) => {
        setResumes(data);
      })
      .catch(() => {
        console.log("error fetching dashboard resumes");
      });
  }, [userId]);

  return (
    <div>
      <div>
        {resumes.map((resume) => (
          <div>
            <ResumeCard
              resumeId={resume.resumeId}
              resumeName={resume.resumeName}
              creationDate={resume.creationDate}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Resumes;
