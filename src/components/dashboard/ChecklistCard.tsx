import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import { MdOutlineDelete, MdOutlineRemoveRedEye } from "react-icons/md";
import classes from "./ChecklistCard.module.css";
import { useEffect, useState } from "react";
import ChecklistItemInterface from "../../interfaces/ChecklistItemInterface";
import ChecklistItem from "../checklist page/checklist item/ChecklistItem";

export const ChecklistCard = (props: {
  checklistId: string;
  checklistName: string;
  checklistStatus: number;
}) => {
  const [showTasks, setShowTasks] = useState(false);
  const [checklistItems, setChecklistItems] = useState<
    ChecklistItemInterface[]
  >([]);

  useEffect(() => {
    fetch(
      `http://localhost:5000/api/ChecklistItems/checklist/${props.checklistId}`
    )
      .then((response) => response.json())
      .then((data) => {
        setChecklistItems(data);
      })
      .catch((error) => {
        console.log("error fetching checklist items: ", error);
      });
  }, [checklistItems]);

  const deleteHandler = () => {
    fetch(`http://localhost:5000/api/Checklists/${props.checklistId}`, {
      method: "DELETE",
    }).catch((error) => console.log(error));
  };

  return (
    <div className={classes.checklist} key={props.checklistId}>
      <div className={classes.container}>
        <div className={classes.left}>
          <div className={classes.progressbar}>
            <CircularProgressbar
              value={props.checklistStatus}
              text={`${props.checklistStatus}%`}
              styles={buildStyles({
                textSize: "2rem",
                pathColor: `rgb(200, 75, 49)`,
                textColor: "#C84B31",
              })}
            />
          </div>
          <div>{props.checklistName}</div>
        </div>
        <div className={classes.actions}>
          <button type="button" onClick={() => setShowTasks(!showTasks)}>
            <MdOutlineRemoveRedEye />
          </button>
          <button type="button" onClick={deleteHandler}>
            <MdOutlineDelete />
          </button>
        </div>
      </div>
      {showTasks && (
        <div className={classes.tasks}>
          {checklistItems.map((item) => (
            <ChecklistItem
              itemId={item.itemId}
              text={item.text}
              status={item.status}
            />
          ))}
        </div>
      )}
    </div>
  );
};

export default ChecklistCard;
