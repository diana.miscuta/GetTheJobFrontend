import { useContext, useEffect, useState } from "react";
import { EditorTemplate } from "./EditorTemplate";
import classes from "./MonthlyView.module.css";
import {
  EventRenderedArgs,
  Inject,
  Month,
  ScheduleComponent,
  ViewDirective,
  ViewsDirective,
} from "@syncfusion/ej2-react-schedule";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import EventInterface from "../../interfaces/EventInterface";

export const MonthlyView = () => {
  const [events, setEvents] = useState<EventInterface[]>([]);
  const [eventType, setEventType] = useState("");
  const { userId } = useContext(GetTheJobContext);

  useEffect(() => {
    fetch(`http://localhost:5000/api/Events/${userId}`)
      .then((response) => response.json())
      .then((data) => {
        setEvents(data);
      })
      .catch(() => {
        console.log("error fetching events");
      });
  }, [eventType]);

  const fieldsData = {
    id: "eventId",
    subject: { name: "eventName" },
    startTime: { name: "eventStartDate" },
    endTime: { name: "eventStopDate" },
    location: { name: "eventLocation" },
    description: { name: "eventDetails" },
  };

  const todayDate = new Date();

  const onEventRendered = (args: EventRenderedArgs) => {
    switch (args.data.eventColor) {
      case 3:
        args.element.style.backgroundColor = "#93ae55";
        break;
      case 2:
        args.element.style.backgroundColor = "#F2D096";
        break;
      case 1:
        args.element.style.backgroundColor = "#e54a50";
        break;
    }
  };

  const getEventPriority = (priority: string) => {
    switch (priority) {
      case "High":
        return 1;
      case "Medium":
        return 2;
      case "Low":
        return 3;
      default:
        return 3;
    }
  };

  const convertTime = (date: Date) => {
    // Convert to ISO string
    const isoString = date.toISOString();

    // Create new Date object from the ISO string and add 3 hours
    const newDate = new Date(isoString);
    newDate.setHours(newDate.getHours() + 3);

    // Return new date as ISO string
    return newDate.toISOString();
  };

  const onActionBegin = async (args: {
    requestType: string;
    data: any[] | any;
    cancel: boolean;
  }) => {
    //Adding a new event
    if (args.requestType === "eventCreate") {
      await fetch("http://localhost:5000/api/Events", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          userId: userId,
          eventName: args.data[0].Title,
          eventStartDate: convertTime(args.data[0].StartTime),
          eventStopDate: convertTime(args.data[0].StopTime),
          eventDetails: args.data[0].Description,
          eventColor: getEventPriority(args.data[0].EventType),
          eventLocation: args.data[0].Location,
        }),
      });
    }

    //Updating an event
    if (args.requestType === "eventChange") {
      const data = args.data;
      await fetch(`http://localhost:5000/api/Events/${data.eventId}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          eventName: data.Title,
          eventStartDate: convertTime(data.StartTime),
          eventStopDate: convertTime(data.StopTime),
          eventDetails: data.Description,
          eventColor: getEventPriority(data.EventType),
          eventLocation: data.Location,
        }),
      });
    }

    //Deleting an event
    if (args.requestType === "eventRemove") {
      fetch(`http://localhost:5000/api/Events/${args.data[0].eventId}`, {
        method: "DELETE",
      }).catch((error) => console.log(error));
    }
    setEventType(args.requestType);
  };

  return (
    <div className={classes.container}>
      <div className="col-lg-9 control-section">
        <div className="control-wrapper">
          <ScheduleComponent
            width="100%"
            height="85vh"
            selectedDate={todayDate}
            eventSettings={{ dataSource: events, fields: fieldsData }}
            eventRendered={onEventRendered.bind(this)}
            editorTemplate={EditorTemplate.bind(this)}
            actionBegin={onActionBegin.bind(this)}
            showQuickInfo={false}
            firstDayOfWeek={1}
          >
            <ViewsDirective>
              <ViewDirective option="Month" />
            </ViewsDirective>
            <Inject services={[Month]} />
          </ScheduleComponent>
        </div>
      </div>
    </div>
  );
};

export default MonthlyView;
