import { DateTimePickerComponent } from "@syncfusion/ej2-react-calendars";
import { DropDownListComponent } from "@syncfusion/ej2-react-dropdowns";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { useState } from "react";

export const EditorTemplate = (props: Record<string, any>) => {
  const priorityLevels: string[] = ["High", "Medium", "Low"];
  const [eventName, setEventName] = useState(props.eventName);
  const [eventLocation, setEventLocation] = useState(props.eventLocation);
  const [eventPriority, setEventPriority] = useState(props.eventColor - 1);
  const [eventStartTime, setEventStartTime] = useState(props.eventStartDate);
  const [eventStopTime, setEventStopTime] = useState(props.eventStopDate);
  const [eventDescription, setEventDescription] = useState(props.eventDetails);

  return props !== undefined ? (
    <table
      className="custom-event-editor"
      style={{ width: "100%" }}
      cellPadding={5}
    >
      <tbody>
        <tr>
          <td colSpan={2}>
            <TextBoxComponent
              id="Title"
              placeholder="Title"
              data-name="eventName"
              floatLabelType="Always"
              className="e-field"
              style={{ width: "100%" }}
              value={eventName}
              onChange={(e: any) => setEventName(e.target.value)}
            />
          </td>
          <td colSpan={2}>
            <TextBoxComponent
              id="Location"
              placeholder="Location"
              data-name="Location"
              floatLabelType="Always"
              className="e-field"
              style={{ width: "100%" }}
              value={eventLocation}
              onChange={(e: any) => setEventLocation(e.target.value)}
            />
          </td>
        </tr>
        <tr>
          <td colSpan={4}>
            <DropDownListComponent
              id="EventType"
              placeholder="Choose event priority"
              data-name="EventType"
              floatLabelType="Always"
              className="e-field"
              style={{ width: "100%" }}
              dataSource={priorityLevels}
              value={priorityLevels[eventPriority]}
              onChange={(e: any) => setEventPriority(e.target.value)}
            ></DropDownListComponent>
          </td>
        </tr>
        <tr>
          <td colSpan={2}>
            <DateTimePickerComponent
              id="StartTime"
              placeholder="From"
              format="dd/MM/yy hh:mm a"
              data-name="StartTime"
              floatLabelType="Always"
              value={eventStartTime}
              onChange={(e: any) => setEventStartTime(e.target.value)}
              className="e-field"
            ></DateTimePickerComponent>
          </td>
          <td colSpan={2}>
            <DateTimePickerComponent
              id="StopTime"
              placeholder="To"
              format="dd/MM/yy hh:mm a"
              data-name="StopTime"
              floatLabelType="Always"
              value={eventStopTime}
              onChange={(e: any) => setEventStopTime(e.target.value)}
              className="e-field"
            ></DateTimePickerComponent>
          </td>
        </tr>
        <tr>
          <td colSpan={4}>
            <textarea
              id="Description"
              placeholder="Description"
              className="e-field e-input"
              name="Description"
              rows={3}
              cols={50}
              style={{
                resize: "none",
              }}
              value={eventDescription}
              onChange={(e: any) => setEventDescription(e.target.value)}
            ></textarea>
          </td>
        </tr>
      </tbody>
    </table>
  ) : (
    <div></div>
  );
};
