import classes from "./WorkExperience.module.css";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { useContext, useEffect, useState } from "react";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { ToastContainer, toast } from "react-toastify";

export const WorkExperience = (props: {
  setIsSaved: (isSaved: boolean) => void;
}) => {
  const temp = {
    resumeId: "",
    workId: "",
    jobTitle: "",
    institution: "",
    jobCountry: "",
    jobCity: "",
    startDate: "",
    endDate: "",
    jobDescription: "",
  };

  const [workExperience, setWorkExperience] = useState(temp);
  const { resumeId } = useContext(GetTheJobContext);
  const [jobTitle, setJobTitle] = useState("");
  const [institution, setInstitution] = useState("");
  const [jobCountry, setJobCountry] = useState("");
  const [jobCity, setJobCity] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [jobDescription, setJobDescription] = useState("");

  const htmlAttributes = { rows: "4" };

  useEffect(() => {
    fetch(`http://localhost:5000/api/WorkExperiences/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setWorkExperience(data);
      })
      .catch(() => {
        console.log("error fetch work experience");
      });
  }, [workExperience, resumeId]);

  const handleSave = async () => {
    if (workExperience.resumeId === "") {
      await fetch("http://localhost:5000/api/WorkExperiences/", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
        body: JSON.stringify({
          resumeId: resumeId,
          jobTitle: jobTitle,
          institution: institution,
          jobCountry: jobCountry,
          jobCity: jobCity,
          startDate: startDate,
          endDate: endDate,
          jobDescription: jobDescription,
        }),
      });
    } else {
      await fetch(
        `http://localhost:5000/api/WorkExperiences/${workExperience.workId}`,
        {
          method: "PUT",
          headers: { "Content-Type": "application/json" },
          credentials: "include",
          body: JSON.stringify({
            jobTitle: jobTitle ? jobTitle : workExperience.jobTitle,
            institution: institution ? institution : workExperience.institution,
            jobCountry: jobCountry ? jobCountry : workExperience.jobCountry,
            jobCity: jobCity ? jobCity : workExperience.jobCity,
            startDate: startDate ? startDate : workExperience.startDate,
            endDate: endDate ? endDate : workExperience.endDate,
            jobDescription: jobDescription
              ? jobDescription
              : workExperience.jobDescription,
          }),
        }
      );
    }
    toast.success("Information saved!", {
      position: "bottom-left",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    props.setIsSaved(true);
  };

  return (
    <div className={classes.container}>
      <h1>Work experience</h1>
      <div className={classes.form}>
        <div className={classes.row}>
          <div className={classes.input}>
            <p>Job title:</p>
            <TextBoxComponent
              placeholder="Job title"
              cssClass="e-outline"
              id="jobTitle"
              value={workExperience.jobTitle}
              onChange={(e: any) => setJobTitle(e.target.value)}
            />
          </div>
          <div className={classes.input}>
            <p>Company name:</p>
            <TextBoxComponent
              placeholder="Company name"
              cssClass="e-outline"
              id="company"
              value={workExperience.institution}
              onChange={(e: any) => setInstitution(e.target.value)}
            />
          </div>
        </div>
        <div className={classes.row}>
          <div className={classes.input}>
            <p>Country:</p>
            <TextBoxComponent
              placeholder="Country"
              cssClass="e-outline"
              id="country"
              value={workExperience.jobCountry}
              onChange={(e: any) => setJobCountry(e.target.value)}
            />
          </div>
          <div className={classes.input}>
            <p>City:</p>
            <TextBoxComponent
              placeholder="City"
              cssClass="e-outline"
              id="city"
              value={workExperience.jobCity}
              onChange={(e: any) => setJobCity(e.target.value)}
            />
          </div>
        </div>
        <div className={classes.time}>
          <p>Time period:</p>
        </div>
        <div className={classes.row}>
          <TextBoxComponent
            placeholder="Start date"
            cssClass="e-outline"
            id="startDate"
            value={workExperience.startDate}
            onChange={(e: any) => setStartDate(e.target.value)}
          />
          <h1>-</h1>
          <TextBoxComponent
            placeholder="End date"
            cssClass="e-outline"
            id="endDate"
            value={workExperience.endDate}
            onChange={(e: any) => setEndDate(e.target.value)}
          />
        </div>
        <div className={classes.textarea}>
          <TextBoxComponent
            id="default"
            multiline={true}
            floatLabelType="Auto"
            placeholder="Say a few words about your experience"
            value={workExperience.jobDescription}
            onChange={(e: any) => setJobDescription(e.target.value)}
            htmlAttributes={htmlAttributes}
          />
        </div>
      </div>
      <div className={classes.actions}>
        <TooltipComponent
          className="tooltip-box"
          content="Do not forget to save before adding another work experience."
          openDelay={500}
        >
          <AiOutlineExclamationCircle className={classes.tooltip} />
        </TooltipComponent>
        <button type="button" onClick={handleSave}>
          Save
        </button>
      </div>
      <ToastContainer />
    </div>
  );
};

export default WorkExperience;
