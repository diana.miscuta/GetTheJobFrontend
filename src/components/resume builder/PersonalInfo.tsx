import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import classes from "./PersonalInfo.module.css";
import { useContext, useEffect, useState } from "react";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { ToastContainer, toast } from "react-toastify";

export const PersonalInfo = (props: {
  setIsSaved: (isSaved: boolean) => void;
}) => {
  const temp = {
    personalInfoId: "",
    resumeId: "",
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    linkedInLink: "",
    address: "",
    description: "",
  };

  const [personalInfo, setPersonalInfo] = useState(temp);
  const { resumeId } = useContext(GetTheJobContext);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [linkedInLink, setLinkedInLink] = useState("");
  const [description, setDescription] = useState("");

  const htmlAttributes = { rows: "4" };

  useEffect(() => {
    fetch(`http://localhost:5000/api/PersonalInfos/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setPersonalInfo(data);
      })
      .catch(() => {
        console.log("error fetch personal info");
      });
  }, [personalInfo, resumeId]);

  const handleSave = async () => {
    if (personalInfo.resumeId === "") {
      await fetch("http://localhost:5000/api/PersonalInfos/", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
        body: JSON.stringify({
          resumeId: resumeId,
          firstName: firstName,
          lastName: lastName,
          phoneNumber: phoneNumber,
          address: address,
          email: email,
          linkedInLink: linkedInLink,
          description: description,
        }),
      });
    } else {
      await fetch(
        `http://localhost:5000/api/PersonalInfos/${personalInfo.personalInfoId}`,
        {
          method: "PUT",
          headers: { "Content-Type": "application/json" },
          credentials: "include",
          body: JSON.stringify({
            firstName: firstName ? firstName : personalInfo.firstName,
            lastName: lastName ? lastName : personalInfo.lastName,
            phoneNumber: phoneNumber ? phoneNumber : personalInfo.phoneNumber,
            address: address ? address : personalInfo.address,
            email: email ? email : personalInfo.email,
            linkedInLink: linkedInLink
              ? linkedInLink
              : personalInfo.linkedInLink,
            description: description ? description : personalInfo.description,
          }),
        }
      );
    }
    toast.success("Information saved!", {
      position: "bottom-left",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    props.setIsSaved(true);
  };

  return (
    <div className={classes.container}>
      <h1 className={classes.title}>Personal information</h1>
      <div className={classes.form}>
        <div className={classes.row}>
          <div className={classes.input}>
            <p>First name:</p>
            <TextBoxComponent
              placeholder="First name"
              cssClass="e-outline"
              id="firstName"
              value={personalInfo.firstName}
              onChange={(e: any) => setFirstName(e.target.value)}
            />
          </div>
          <div className={classes.input}>
            <p>Last name:</p>
            <TextBoxComponent
              placeholder="Last name"
              cssClass="e-outline"
              id="lastName"
              value={personalInfo.lastName}
              onChange={(e: any) => setLastName(e.target.value)}
            />
          </div>
        </div>
        <div className={classes.row}>
          <div className={classes.input}>
            <p>Email:</p>
            <TextBoxComponent
              placeholder="Email"
              cssClass="e-outline"
              id="email"
              type="email"
              value={personalInfo.email}
              onChange={(e: any) => setEmail(e.target.value)}
            />
          </div>
          <div className={classes.input}>
            <p>LinkedIn:</p>
            <TextBoxComponent
              placeholder="LinkedIn Link"
              cssClass="e-outline"
              id="linkedin"
              value={personalInfo.linkedInLink}
              onChange={(e: any) => setLinkedInLink(e.target.value)}
            />
          </div>
        </div>
        <div className={classes.row}>
          <div className={classes.input}>
            <p>Adress:</p>
            <TextBoxComponent
              placeholder="Address"
              cssClass="e-outline"
              id="address"
              value={personalInfo.address}
              onChange={(e: any) => setAddress(e.target.value)}
            />
          </div>
          <div className={classes.input}>
            <p>Phone number:</p>
            <TextBoxComponent
              placeholder="Phone number"
              cssClass="e-outline"
              id="number"
              value={personalInfo.phoneNumber}
              onChange={(e: any) => setPhoneNumber(e.target.value)}
            />
          </div>
        </div>
        <div className={classes.textarea}>
          <TextBoxComponent
            id="default"
            multiline={true}
            floatLabelType="Auto"
            placeholder="Write some words about yourself"
            value={personalInfo.description}
            onChange={(e: any) => setDescription(e.target.value)}
            htmlAttributes={htmlAttributes}
          />
        </div>
      </div>
      <div className={classes.actions}>
        <TooltipComponent
          className="tooltip-box"
          content="Do not forget to save before moving to the next section."
          openDelay={1000}
        >
          <AiOutlineExclamationCircle className={classes.tooltip} />
        </TooltipComponent>
        <button type="button" onClick={handleSave}>
          Save
        </button>
      </div>
      <ToastContainer />
    </div>
  );
};

export default PersonalInfo;
