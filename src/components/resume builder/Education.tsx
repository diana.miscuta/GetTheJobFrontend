import classes from "./Education.module.css";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { useContext, useEffect, useState } from "react";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { ToastContainer, toast } from "react-toastify";

export const Education = (props: {
  setIsSaved: (isSaved: boolean) => void;
}) => {
  const temp = {
    educationId: "",
    resumeId: "",
    studyField: "",
    institution: "",
    educationCountry: "",
    educationCity: "",
    startDate: "",
    endDate: "",
    educationDescription: "",
  };

  const [education, setEducation] = useState(temp);
  const { resumeId } = useContext(GetTheJobContext);
  const [institution, setInstitution] = useState("");
  const [studyField, setStudyField] = useState("");
  const [educationCountry, setEducationCountry] = useState("");
  const [educationCity, setEducationCity] = useState("");
  const [startDate, setStartDate] = useState("2023-06");
  const [endDate, setEndDate] = useState("2023-06");
  const [educationDescription, setEducationDescription] = useState("");

  const htmlAttributes = { rows: "4" };

  useEffect(() => {
    fetch(`http://localhost:5000/api/Educations/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setEducation(data);
      })
      .catch(() => {
        console.log("error fetch education");
      });
  }, [education, resumeId]);

  const handleSave = async () => {
    if (education.resumeId === "") {
      await fetch("http://localhost:5000/api/Educations/", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
        body: JSON.stringify({
          resumeId: resumeId,
          studyField: studyField,
          institution: institution,
          educationCountry: educationCountry,
          educationCity: educationCity,
          startDate: startDate,
          endDate: endDate,
          educationDescription: educationDescription,
        }),
      });
    } else {
      await fetch(
        `http://localhost:5000/api/Educations/${education.educationId}`,
        {
          method: "PUT",
          headers: { "Content-Type": "application/json" },
          credentials: "include",
          body: JSON.stringify({
            studyField: studyField ? studyField : education.studyField,
            institution: institution ? institution : education.institution,
            educationCountry: educationCountry
              ? educationCountry
              : education.educationCountry,
            educationCity: educationCity
              ? educationCity
              : education.educationCity,
            startDate: startDate ? startDate : education.startDate,
            endDate: endDate ? endDate : education.endDate,
            educationDescription: educationDescription
              ? educationDescription
              : education.educationDescription,
          }),
        }
      );
    }
    toast.success("Information saved!", {
      position: "bottom-left",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    props.setIsSaved(true);
  };

  return (
    <div className={classes.container}>
      <h1>Education</h1>
      <div className={classes.form}>
        <div className={classes.row}>
          <div className={classes.input}>
            <p>Institution name:</p>
            <TextBoxComponent
              placeholder="Institution name"
              cssClass="e-outline"
              id="institution"
              value={education.institution}
              onChange={(e: any) => setInstitution(e.target.value)}
            />
          </div>
          <div className={classes.input}>
            <p>Field of study:</p>
            <TextBoxComponent
              placeholder="Field of study"
              cssClass="e-outline"
              id="field"
              value={education.studyField}
              onChange={(e: any) => setStudyField(e.target.value)}
            />
          </div>
        </div>
        <div className={classes.row}>
          <div className={classes.input}>
            <p>Country:</p>
            <TextBoxComponent
              placeholder="Country"
              cssClass="e-outline"
              id="country"
              value={education.educationCountry}
              onChange={(e: any) => setEducationCountry(e.target.value)}
            />
          </div>
          <div className={classes.input}>
            <p>City:</p>
            <TextBoxComponent
              placeholder="City"
              cssClass="e-outline"
              id="city"
              value={education.educationCity}
              onChange={(e: any) => setEducationCity(e.target.value)}
            />
          </div>
        </div>
        <div className={classes.time}>
          <p>Time period:</p>
        </div>
        <div className={classes.row}>
          <TextBoxComponent
            placeholder="Start date"
            cssClass="e-outline"
            id="startDate"
            value={education.startDate}
            onChange={(e: any) => setStartDate(e.target.value)}
          />
          <h1>-</h1>
          <TextBoxComponent
            placeholder="End date"
            cssClass="e-outline"
            id="endDate"
            value={education.endDate}
            onChange={(e: any) => setEndDate(e.target.value)}
          />
        </div>
        <div className={classes.textarea}>
          <TextBoxComponent
            id="default"
            multiline={true}
            floatLabelType="Auto"
            placeholder="Say a few words about what you studied"
            value={education.educationDescription}
            onChange={(e: any) => setEducationDescription(e.target.value)}
            htmlAttributes={htmlAttributes}
          />
        </div>
      </div>
      <div className={classes.actions}>
        <TooltipComponent
          className="tooltip-box"
          content="Do not forget to save before adding another education."
          openDelay={500}
        >
          <AiOutlineExclamationCircle className={classes.tooltip} />
        </TooltipComponent>
        <button type="button" onClick={handleSave}>
          Save
        </button>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Education;
