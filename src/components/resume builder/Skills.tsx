import classes from "./Education.module.css";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { useContext, useEffect, useState } from "react";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { ToastContainer, toast } from "react-toastify";

export const Skills = (props: { setIsSaved: (isSaved: boolean) => void }) => {
  const temp = [
    { skillId: "", resumeId: "", skillName: "" },
    { skillId: "", resumeId: "", skillName: "" },
    { skillId: "", resumeId: "", skillName: "" },
    { skillId: "", resumeId: "", skillName: "" },
    { skillId: "", resumeId: "", skillName: "" },
    { skillId: "", resumeId: "", skillName: "" },
  ];
  const [skills, setSkills] = useState(temp);
  const { resumeId } = useContext(GetTheJobContext);
  const [skill1, setSkill1] = useState("");
  const [skill2, setSkill2] = useState("");
  const [skill3, setSkill3] = useState("");
  const [skill4, setSkill4] = useState("");
  const [skill5, setSkill5] = useState("");
  const [skill6, setSkill6] = useState("");

  useEffect(() => {
    fetch(`http://localhost:5000/api/Skills/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setSkills(data);
      })
      .catch(() => {
        console.log("error fetch skills");
      });
  }, [skills, resumeId]);

  const handleSave = async () => {
    if (skills[0].resumeId === "") {
      await fetch("http://localhost:5000/api/Skills/", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
        body: JSON.stringify([
          { resumeId: resumeId, skillName: skill1 },
          { resumeId: resumeId, skillName: skill2 },
          { resumeId: resumeId, skillName: skill3 },
          { resumeId: resumeId, skillName: skill4 },
          { resumeId: resumeId, skillName: skill5 },
          { resumeId: resumeId, skillName: skill6 },
        ]),
      });
    } else {
      await fetch("http://localhost:5000/api/Skills/", {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
        body: JSON.stringify([
          {
            skillId: skills[0].skillId,
            skillName: skill1 ? skill1 : skills[0].skillName,
          },
          {
            skillId: skills[1].skillId,
            skillName: skill2 ? skill2 : skills[1].skillName,
          },
          {
            skillId: skills[2].skillId,
            skillName: skill3 ? skill3 : skills[2].skillName,
          },
          {
            skillId: skills[3].skillId,
            skillName: skill4 ? skill4 : skills[3].skillName,
          },
          {
            skillId: skills[4].skillId,
            skillName: skill5 ? skill5 : skills[4].skillName,
          },
          {
            skillId: skills[5].skillId,
            skillName: skill6 ? skill6 : skills[5].skillName,
          },
        ]),
      });
    }
    toast.success("Information saved!", {
      position: "bottom-left",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    props.setIsSaved(true);
  };

  return (
    <div className={classes.container}>
      <h1>Skills</h1>
      <div className={classes.form}>
        <div className={classes.row}>
          <div className={classes.input}>
            <div className={classes.row}>
              <TextBoxComponent
                placeholder="Skill 1"
                cssClass="e-outline"
                id="institution"
                value={skills[0].skillName}
                onChange={(e: any) => setSkill1(e.target.value)}
              />
              <TextBoxComponent
                placeholder="Skill 2"
                cssClass="e-outline"
                id="institution"
                value={skills[1].skillName}
                onChange={(e: any) => setSkill2(e.target.value)}
              />
            </div>
            <div className={classes.row}>
              <TextBoxComponent
                placeholder="Skill 3"
                cssClass="e-outline"
                id="skill3"
                value={skills[2].skillName}
                onChange={(e: any) => setSkill3(e.target.value)}
              />
              <TextBoxComponent
                placeholder="Skill 4"
                cssClass="e-outline"
                id="skill4"
                value={skills[3].skillName}
                onChange={(e: any) => setSkill4(e.target.value)}
              />
            </div>
            <div className={classes.row}>
              <TextBoxComponent
                placeholder="Skill 5"
                cssClass="e-outline"
                id="skill5"
                value={skills[4].skillName}
                onChange={(e: any) => setSkill5(e.target.value)}
              />
              <TextBoxComponent
                placeholder="Skill 6"
                cssClass="e-outline"
                id="skill6"
                value={skills[5].skillName}
                onChange={(e: any) => setSkill6(e.target.value)}
              />
            </div>
          </div>
        </div>
      </div>
      <div className={classes.actions}>
        <TooltipComponent
          className="tooltip-box"
          content="Do not forget to save before moving to the next section."
          openDelay={500}
        >
          <AiOutlineExclamationCircle className={classes.tooltip} />
        </TooltipComponent>
        <button type="button" onClick={handleSave}>
          Save
        </button>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Skills;
