import classes from "./Education.module.css";
import { TextBoxComponent } from "@syncfusion/ej2-react-inputs";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import { useContext, useEffect, useState } from "react";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { GetTheJobContext } from "../../context/GetTheJobContext";
import { ToastContainer, toast } from "react-toastify";

export const Languages = (props: {
  setIsSaved: (isSaved: boolean) => void;
}) => {
  const temp = [
    { languageId: "", resumeId: "", languageName: "", languageLevel: "" },
    { languageId: "", resumeId: "", languageName: "", languageLevel: "" },
    { languageId: "", resumeId: "", languageName: "", languageLevel: "" },
  ];

  const [languages, setLanguages] = useState(temp);
  const { resumeId } = useContext(GetTheJobContext);
  const [language1, setLanguage1] = useState("");
  const [language2, setLanguage2] = useState("");
  const [language3, setLanguage3] = useState("");
  const [languageLevel1, setLanguageLevel1] = useState("");
  const [languageLevel2, setLanguageLevel2] = useState("");
  const [languageLevel3, setLanguageLevel3] = useState("");

  useEffect(() => {
    fetch(`http://localhost:5000/api/Languages/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setLanguages(data);
      })
      .catch(() => {
        console.log("error fetch languages");
      });
  }, [languages, resumeId]);

  const handleSave = async () => {
    if (languages[0].resumeId === "") {
      await fetch("http://localhost:5000/api/Languages/", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
        body: JSON.stringify([
          {
            resumeId: resumeId,
            languageName: language1,
            languageLevel: languageLevel1,
          },
          {
            resumeId: resumeId,
            languageName: language2,
            languageLevel: languageLevel2,
          },
          {
            resumeId: resumeId,
            languageName: language3,
            languageLevel: languageLevel3,
          },
        ]),
      });
    } else {
      await fetch("http://localhost:5000/api/Languages/", {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
        body: JSON.stringify([
          {
            languageId: languages[0].languageId,
            languageName: language1 ? language1 : languages[0].languageName,
            languageLevel: languageLevel1
              ? languageLevel1
              : languages[0].languageLevel,
          },
          {
            languageId: languages[1].languageId,
            languageName: language2 ? language2 : languages[1].languageName,
            languageLevel: languageLevel2
              ? languageLevel2
              : languages[1].languageLevel,
          },
          {
            languageId: languages[2].languageId,
            languageName: language3 ? language3 : languages[2].languageName,
            languageLevel: languageLevel3
              ? languageLevel3
              : languages[2].languageLevel,
          },
        ]),
      });
    }
    toast.success("Information saved!", {
      position: "bottom-left",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    props.setIsSaved(true);
  };

  return (
    <div className={classes.container}>
      <h1>Languages</h1>
      <div className={classes.form}>
        <div className={classes.row}>
          <div className={classes.input}>
            <div className={classes.row}>
              <TextBoxComponent
                placeholder="Language 1"
                cssClass="e-outline"
                id="language1"
                value={languages[0].languageName}
                onChange={(e: any) => setLanguage1(e.target.value)}
              />
              <TextBoxComponent
                placeholder="Level"
                cssClass="e-outline"
                id="level1"
                value={languages[0].languageLevel}
                onChange={(e: any) => setLanguageLevel1(e.target.value)}
              />
            </div>
            <div className={classes.row}>
              <TextBoxComponent
                placeholder="Language 2"
                cssClass="e-outline"
                id="language2"
                value={languages[1].languageName}
                onChange={(e: any) => setLanguage2(e.target.value)}
              />
              <TextBoxComponent
                placeholder="Level"
                cssClass="e-outline"
                id="level2"
                value={languages[1].languageLevel}
                onChange={(e: any) => setLanguageLevel2(e.target.value)}
              />
            </div>
            <div className={classes.row}>
              <TextBoxComponent
                placeholder="Language 3"
                cssClass="e-outline"
                id="language3"
                value={languages[2].languageName}
                onChange={(e: any) => setLanguage3(e.target.value)}
              />
              <TextBoxComponent
                placeholder="Level"
                cssClass="e-outline"
                id="level3"
                value={languages[2].languageLevel}
                onChange={(e: any) => setLanguageLevel3(e.target.value)}
              />
            </div>
          </div>
        </div>
      </div>
      <div className={classes.actions}>
        <TooltipComponent
          className="tooltip-box"
          content="Do not forget to save before moving to the next section."
          openDelay={500}
        >
          <AiOutlineExclamationCircle className={classes.tooltip} />
        </TooltipComponent>
        <button type="button" onClick={handleSave}>
          Save
        </button>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Languages;
