import classes from "./AboutUs.module.css";

export const AboutUs = (props: { id: string | undefined }) => {
  return (
    <div id={props.id} className={classes.container}>
      <div className={classes.image}>
        <img src={require("../../../resources/about_us.jpg")} alt="about us" />
      </div>
      <div className={classes.text}>
        <p className={classes.info}>
          At GetTheJob, the challenges and frustrations that job seekers face
          are heard and understood. This application was developed by a student
          who personally experienced these difficulties and sought to create a
          solution to streamline the job hunting process.
        </p>
        <p className={classes.info}>
          My goal is to provide a user-friendly platform that empowers
          individuals in their job search journey. I have put our heart and soul
          into crafting a comprehensive tool that addresses the pain points
          faced by job seekers. Join me on this mission as I strive to make your
          job hunting experience smoother, more organized, and ultimately, more
          successful.
        </p>
      </div>
    </div>
  );
};

export default AboutUs;
