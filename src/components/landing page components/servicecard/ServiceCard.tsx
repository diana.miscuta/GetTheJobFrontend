import classes from "./ServiceCard.module.css";

export const ServiceCard = (props: { path: string; description: string }) => {
  return (
    <div className={classes.container}>
      <div className={classes.image}>
        <img src={require(`../../../resources/${props.path}`)} />
      </div>
      <div className={classes.description}>{props.description}</div>
    </div>
  );
};

export default ServiceCard;
