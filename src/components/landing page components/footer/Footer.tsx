import { Link } from "react-scroll";
import classes from "./Footer.module.css";
import { BsGithub, BsFacebook, BsLinkedin } from "react-icons/bs";

export const Footer = () => {
  return (
    <div className={classes.container}>
      <div className={classes.logo}>
        <img src={require("../../../resources/getthejoblogo.png")} />
      </div>

      <div className={classes.links}>
        <ul>
          <li>
            <BsLinkedin />
          </li>
          <li>
            <BsGithub />
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Footer;
