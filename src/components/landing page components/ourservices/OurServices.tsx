import ServiceCard from "../servicecard/ServiceCard";
import classes from "./OurServices.module.css";

export const OurServices = (props: { id: string | undefined }) => {
  const ourservices = [
    {
      imagePath: "calendar.png",
      description:
        "Calendar to keep track of your time. Never miss an interviews again, just add it to your calendar and we will make sure you remember.",
    },
    {
      imagePath: "forum.png",
      description:
        "Support for technical interviews. See resources added by other users and add your resources to help out others in need.",
    },
    {
      imagePath: "checklist.png",
      description:
        "Checklists for getting everything done. Write down, prioritize and get things done without forgetting.",
    },
  ];

  return (
    <div id={props.id} className={classes.container}>
      <div className={classes.title}>Our services</div>
      <div className={classes.cards}>
        {ourservices.map((service) => (
          <ServiceCard
            path={service.imagePath}
            description={service.description}
          />
        ))}
      </div>
    </div>
  );
};

export default OurServices;
