import { Link } from "react-router-dom";
import classes from "./CallToAction.module.css";
import { FiUserPlus } from "react-icons/fi";

export const CallToAction = () => {
  return (
    <div className={classes.container}>
      <div className={classes.header}>
        Start using <b>Get The Job</b> right now!
      </div>
      <div className={classes.info}>
        Bring some order into the chaos, make your life easier.
      </div>
      <Link to="/signup" className={classes.linksignup}>
        <div className={classes.signup}>
          <p>Register</p>
          <FiUserPlus className={classes.icon} />
        </div>
      </Link>
    </div>
  );
};

export default CallToAction;
