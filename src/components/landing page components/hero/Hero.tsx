import classes from "./Hero.module.css";
import { Link } from "react-router-dom";
import { Link as LinkScroll } from "react-scroll";

export const Hero = () => {
  return (
    <div className={classes.container}>
      <div className={classes.actions}>
        <div className={classes.motto}>
          Looking for a job is chaotic?
          <br />
          Let us bring some order!
        </div>
        <LinkScroll
          activeClass="active"
          to="aboutus"
          spy={true}
          smooth={true}
          offset={-70}
          duration={500}
        >
          <button>Read more</button>
        </LinkScroll>
      </div>
    </div>
  );
};
export default Hero;
