import { Link } from "react-router-dom";
import classes from "./MainNavigation.module.css";
import { MdLogout } from "react-icons/md";
import { GetTheJobContext } from "../../../context/GetTheJobContext";
import { useContext } from "react";

export const MainNavigation = () => {
  const { setId, setUserEmail, setFirstName, setLastName } =
    useContext(GetTheJobContext);
  const logout = async () => {
    await fetch("http://localhost:5000/api/Users/Logout", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });
    setId("");
    setUserEmail("");
    setFirstName("");
    setLastName("");
  };

  return (
    <div>
      <header className={classes.header}>
        <div className={classes.logo}>
          <img
            src={require("../../../resources/getthejoblogo.png")}
            alt="logo"
          />
        </div>
        <nav>
          <ul>
            <li>
              <Link to="/">Dashboard</Link>
            </li>
          </ul>
        </nav>
        <Link to="/login" className={classes.linklogout} onClick={logout}>
          <div className={classes.logout}>
            <p>Log out</p>
          </div>
        </Link>
      </header>
    </div>
  );
};

export default MainNavigation;
