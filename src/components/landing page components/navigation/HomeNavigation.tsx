import { Link } from "react-scroll";
import { Link as LinkRouter } from "react-router-dom";
import classes from "./HomeNavigation.module.css";

export const HomeNavigation = () => {
  return (
    <div>
      <header className={classes.header}>
        <LinkRouter to="/" className={classes.logo}>
          <img
            src={require("../../../resources/getthejoblogo.png")}
            alt="logo"
          />
        </LinkRouter>
        <nav>
          <ul>
            <li>
              <Link
                activeClass="active"
                to="aboutus"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                About us
              </Link>
            </li>
            <li>
              <Link
                activeClass="active"
                to="ourservices"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                Our services
              </Link>
            </li>
          </ul>
        </nav>
        <div>
          <LinkRouter to="/login" className={classes.link}>
            <button className={classes.login}>Sign in</button>
          </LinkRouter>
          <LinkRouter to="/signup" className={classes.link}>
            <button className={classes.register}>Sign up</button>
          </LinkRouter>
        </div>
      </header>
    </div>
  );
};

export default HomeNavigation;
