import MonthlyView from "../components/calendar page/MonthlyView";
import classes from "./style/CalendarPage.module.css";

export const CalendarPage = () => {
  return (
    <div className={classes.container}>
      <div className={classes.calendar}>
        <MonthlyView />
      </div>
    </div>
  );
};

export default CalendarPage;
