import LoginForm from "../components/login page/login form/LoginForm";
import classes from "./style/Login.module.css";

export const Login = () => {
  return (
    <div className={classes.container}>
      <div className={classes.image}>
        <img src={require("../resources/login.jpg")} alt="login" />
      </div>
      <div className={classes.form}>
        <LoginForm />
      </div>
    </div>
  );
};

export default Login;
