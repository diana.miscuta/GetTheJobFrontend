import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import classes from "./style/ResumePreviewPage.module.css";
import { useContext, useEffect, useState } from "react";
import { GetTheJobContext } from "../context/GetTheJobContext";
import { MdEmail, MdLocationOn, MdPhone } from "react-icons/md";
import { RiLinkedinFill } from "react-icons/ri";
import { useNavigate } from "react-router-dom";

export const ResumePreviewPage = () => {
  const { resumeId } = useContext(GetTheJobContext);

  //fetching resume information
  const [resumeName, setResumeName] = useState("");
  useEffect(() => {
    fetch(`http://localhost:5000/api/Resumes/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setResumeName(data.resumeName);
      })
      .catch(() => {
        console.log("error fetch resume name");
      });
  }, [resumeName, resumeId]);

  //fetching personal information
  const tempPersonalInfo = {
    personalInfoId: 0,
    resumeId: 0,
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    linkedInLink: "",
    address: "",
    description: "",
  };
  const [personalInfo, setPersonalInfo] = useState(tempPersonalInfo);
  useEffect(() => {
    fetch(`http://localhost:5000/api/PersonalInfos/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setPersonalInfo(data);
      })
      .catch(() => {
        console.log("error fetch personal info");
      });
  }, [personalInfo, resumeId]);

  //fetching skills information
  const tempSkills = [
    { skillId: 0, resumeId: 0, skillName: "" },
    { skillId: 0, resumeId: 0, skillName: "" },
    { skillId: 0, resumeId: 0, skillName: "" },
    { skillId: 0, resumeId: 0, skillName: "" },
    { skillId: 0, resumeId: 0, skillName: "" },
    { skillId: 0, resumeId: 0, skillName: "" },
  ];
  const [skills, setSkills] = useState(tempSkills);
  useEffect(() => {
    fetch(`http://localhost:5000/api/Skills/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setSkills(data);
      })
      .catch(() => {
        console.log("error fetch skills");
      });
  }, [skills, resumeId]);

  //fetching languages information
  const tempLanguages = [
    { languageId: 0, resumeId: 0, languageName: "", languageLevel: "" },
    { languageId: 0, resumeId: 0, languageName: "", languageLevel: "" },
    { languageId: 0, resumeId: 0, languageName: "", languageLevel: "" },
  ];
  const [languages, setLanguages] = useState(tempLanguages);
  useEffect(() => {
    fetch(`http://localhost:5000/api/Languages/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setLanguages(data);
      })
      .catch(() => {
        console.log("error fetch languages");
      });
  }, [languages, resumeId]);

  //fetching work experience information
  const tempWorkExperience = {
    resumeId: 0,
    workId: 0,
    jobTitle: "",
    institution: "",
    jobCountry: "",
    jobCity: "",
    startDate: "",
    endDate: "",
    jobDescription: "",
  };
  const [workExperience, setWorkExperience] = useState(tempWorkExperience);
  useEffect(() => {
    fetch(`http://localhost:5000/api/WorkExperiences/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setWorkExperience(data);
      })
      .catch(() => {
        console.log("error fetch work experience");
      });
  }, [workExperience, resumeId]);

  //fetching education information
  const tempEducation = {
    educationId: 0,
    resumeId: 0,
    studyField: "",
    institution: "",
    educationCountry: "",
    educationCity: "",
    startDate: "",
    endDate: "",
    educationDescription: "",
  };
  const [education, setEducation] = useState(tempEducation);
  useEffect(() => {
    fetch(`http://localhost:5000/api/Educations/${resumeId}`)
      .then((response) => response.json())
      .then((data) => {
        setEducation(data);
      })
      .catch(() => {
        console.log("error fetch education");
      });
  }, [education, resumeId]);

  //download function
  const handleDownload = () => {
    const input = document.getElementById("divToPdf");
    if (input) {
      html2canvas(input).then((canvas) => {
        const imgData = canvas.toDataURL("image/png");
        const pdf = new jsPDF();
        pdf.addImage(imgData, "JPEG", 10, 10, -200, -200);
        pdf.save(`${resumeName}.pdf`);
      });
    }
  };

  const navigate = useNavigate();
  const handleBack = () => {
    navigate("/resumes");
  };

  return (
    <div className={classes.container}>
      <div className={classes.actions}>
        <button type="button" className={classes.back} onClick={handleBack}>
          Back
        </button>
        <button
          type="button"
          className={classes.download}
          onClick={handleDownload}
        >
          Download
        </button>
      </div>
      <div id="divToPdf" className={classes.divToPdf}>
        <div className={classes.header}>
          <p className={classes.name}>
            {personalInfo.firstName} {personalInfo.lastName}
          </p>
          <p className={classes.description}>{personalInfo.description}</p>
        </div>
        <div className={classes.middle}>
          <div className={classes.left}>
            <div className={classes.personalInfo}>
              <p className={classes.sectionHeading}>CONTACT</p>
              <ul>
                <li>
                  <MdPhone className={classes.icon} />{" "}
                  {personalInfo.phoneNumber}
                </li>
                <li>
                  <MdEmail className={classes.icon} /> {personalInfo.email}
                </li>
                <li>
                  <RiLinkedinFill className={classes.icon} />{" "}
                  {personalInfo.linkedInLink}
                </li>
                <li>
                  <MdLocationOn className={classes.icon} />{" "}
                  {personalInfo.address}
                </li>
              </ul>
            </div>
            <div className={classes.bulletPointList}>
              <p className={classes.sectionHeading}>SKILLS</p>
              <ul>
                {skills.map((skill) => (
                  <li>{skill.skillName}</li>
                ))}
              </ul>
            </div>
            <div className={classes.bulletPointList}>
              <p className={classes.sectionHeading}>LANGUAGES</p>
              <ul>
                {languages.map((language) => (
                  <li>
                    {language.languageName} - {language.languageLevel}
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className={classes.right}>
            <div className={classes.workExperience}>
              <p className={classes.sectionHeading}>WORK EXPERIENCE</p>
              <p className={classes.jobTitle}>{workExperience.jobTitle}</p>
              <p className={classes.company}>{workExperience.institution}</p>
              <div className={classes.details}>
                <p className={classes.dateRange}>
                  {workExperience.startDate.toUpperCase()} -{" "}
                  {workExperience.endDate.toUpperCase()}
                </p>
                <p className={classes.dateRange}>
                  {workExperience.jobCity.toUpperCase()},
                  {workExperience.jobCountry.toUpperCase()}
                </p>
              </div>
              <p className={classes.jobDescription}>
                {workExperience.jobDescription}
              </p>
            </div>
            <div className={classes.education}>
              <p className={classes.sectionHeading}>EDUCATION</p>
              <p className={classes.jobTitle}>{education.studyField}</p>
              <p className={classes.company}>{education.institution}</p>
              <div className={classes.details}>
                <p className={classes.dateRange}>
                  {education.startDate.toUpperCase()} -{" "}
                  {education.endDate.toUpperCase()}
                </p>
                <p className={classes.dateRange}>
                  {education.educationCity.toUpperCase()},
                  {education.educationCountry.toUpperCase()}
                </p>
              </div>
              <p className={classes.jobDescription}>
                {education.educationDescription}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ResumePreviewPage;
