import { Link } from "react-router-dom";
import classes from "./style/Dashboard.module.css";
import {
  BsCalendar3,
  BsCardChecklist,
  BsFileEarmarkPerson,
} from "react-icons/bs";
import { MdForum } from "react-icons/md";
import { RiAdminLine } from "react-icons/ri";
import { GetTheJobContext } from "../context/GetTheJobContext";
import { useContext } from "react";
import AgendaView from "../components/dashboard/AgendaView";
import { Goals } from "../components/dashboard/Goals";
import Resumes from "../components/dashboard/Resumes";
import Checklist from "../components/checklist page/checklist/Checklist";
import Checklists from "../components/dashboard/Checklists";

export const Dashboard = () => {
  const { userFirstName, userLastName, userEmail } =
    useContext(GetTheJobContext);
  return (
    <div className={classes.container}>
      <div className={classes.sidebar}>
        <div className={classes.image}>
          <img src={require("../resources/dashboard.jpg")} alt="dashboard" />
          <p>
            {userFirstName} {userLastName}
          </p>
        </div>
        <ul>
          <li>
            <Link to="/checklists">
              <div className={classes.menuitem}>
                <BsCardChecklist />
                Checklists
              </div>
            </Link>
          </li>
          <li>
            <Link to="/calendar">
              <div className={classes.menuitem}>
                <BsCalendar3 />
                Calendar
              </div>
            </Link>
          </li>
          <li>
            <Link to="/resumes">
              <div className={classes.menuitem}>
                <BsFileEarmarkPerson />
                Resumes
              </div>
            </Link>
          </li>
          <li>
            <Link to="/forum">
              <div className={classes.menuitem}>
                <MdForum />
                Forum
              </div>
            </Link>
          </li>
          {userEmail === "admin@admin.com" && (
            <li>
              <Link to="/admin">
                <div className={classes.menuitem}>
                  <RiAdminLine />
                  Admin dashboard
                </div>
              </Link>
            </li>
          )}
        </ul>
      </div>
      <div className={classes.checklists}>
        <Checklists />
      </div>
      <div className={classes.resumes}>
        <Resumes />
      </div>
      <div className={classes.goals}>
        <Goals />
      </div>
      <div className={classes.events}>
        <AgendaView />
      </div>
    </div>
  );
};

export default Dashboard;
