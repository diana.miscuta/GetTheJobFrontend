import { useContext, useEffect, useState } from "react";
import PostCard from "../components/forum/PostCard";
import classes from "./style/AdminDashboard.module.css";
import { GetTheJobContext } from "../context/GetTheJobContext";
import SectionCard from "../components/forum/SectionCard";
import RequestedSectionInterface from "../interfaces/RequestedSectionInterface";
import PostInterface from "../interfaces/PostInterface";
import SectionInterface from "../interfaces/SectionInterface";

export const AdminDashboard = () => {
  const { sectionId, setSectionId } = useContext(GetTheJobContext);

  const [sections, setSections] = useState<SectionInterface[]>([]);
  useEffect(() => {
    fetch("http://localhost:5000/api/Sections/published")
      .then((response) => response.json())
      .then((responseContent) => {
        setSections(responseContent);
      })
      .catch(() => {
        console.log("error fetch published sections");
      });
  }, [sections]);

  const [posts, setPosts] = useState<PostInterface[]>([]);
  useEffect(() => {
    fetch(`http://localhost:5000/api/Posts/pending/${sectionId}`)
      .then((response) => response.json())
      .then((responseContent) => {
        setPosts(responseContent);
      })
      .catch(() => {
        console.log("error fetch pending posts");
      });
  }, [sectionId, posts]);

  const [requestedSections, setRequestedSections] = useState<
    RequestedSectionInterface[]
  >([]);
  useEffect(() => {
    fetch("http://localhost:5000/api/Sections/pending")
      .then((response) => response.json())
      .then((responseContent) => {
        setRequestedSections(responseContent);
      })
      .catch(() => {
        console.log("error fetch pending sections");
      });
  }, [requestedSections]);

  const approveSectionHandler = async (sectionId: string) => {
    await fetch(`http://localhost:5000/api/Sections/${sectionId}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
      body: JSON.stringify({
        status: true,
      }),
    });
  };

  const deleteSectionHandler = (sectionId: string) => {
    fetch(`http://localhost:5000/api/Sections/${sectionId}`, {
      method: "DELETE",
    }).catch((error) => console.log(error));
  };

  const approvePostHandler = async (postId: string) => {
    await fetch(`http://localhost:5000/api/Posts/${postId}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
      body: JSON.stringify({
        status: true,
      }),
    });
  };

  const deletePostHandler = (postId: string) => {
    fetch(`http://localhost:5000/api/Posts/${postId}`, {
      method: "DELETE",
    }).catch((error) => console.log(error));
  };

  return (
    <div>
      <div className={classes.container}>
        <div className={classes.sidebar}>
          <ul>
            <li
              onClick={() => {
                setSectionId("");
              }}
              className={classes.requestedSection}
            >
              Requested sections
            </li>
            {sections.map((section) => (
              <li
                key={section.sectionId}
                onClick={() => setSectionId(section.sectionId)}
              >
                {section.sectionName}
              </li>
            ))}
          </ul>
        </div>
        <div className={classes.posts}>
          <div className={classes.list}>
            {sectionId != "" &&
              posts.map((post) => (
                <div className={classes.post}>
                  <PostCard
                    postId={post.postId}
                    userFirstName={post.userFirstName}
                    userLastName={post.userLastName}
                    postedDate={post.postedDate}
                    postText={post.postText}
                  />
                  <div className={classes.actions}>
                    <button
                      type="button"
                      className={classes.approve}
                      onClick={() => approvePostHandler(post.postId)}
                    >
                      Approve
                    </button>
                    <button
                      type="button"
                      className={classes.delete}
                      onClick={() => deletePostHandler(post.postId)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              ))}
            {sectionId == "" &&
              requestedSections.map((section) => (
                <div className={classes.section}>
                  <SectionCard
                    sectionId={section.sectionId}
                    userFirstName={section.userFirstName}
                    userLastName={section.userLastName}
                    sectionName={section.sectionName}
                  />
                  <div className={classes.actions}>
                    <button
                      type="button"
                      className={classes.approve}
                      onClick={() => approveSectionHandler(section.sectionId)}
                    >
                      Approve
                    </button>
                    <button
                      type="button"
                      className={classes.delete}
                      onClick={() => deleteSectionHandler(section.sectionId)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminDashboard;
