import SignupForm from "../components/signup page/signup form/SignupForm";
import classes from "./style/Signup.module.css";

export const Signup = () => {
  return (
    <div className={classes.container}>
      <div className={classes.form}>
        <SignupForm />
      </div>
      <div className={classes.image}>
        <img src={require("../resources/signup.jpg")} alt="register" />
      </div>
    </div>
  );
};

export default Signup;
