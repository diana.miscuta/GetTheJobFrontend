import AboutUs from "../components/landing page components/aboutus/AboutUs";
import CallToAction from "../components/landing page components/calltoaction/CallToAction";
import Footer from "../components/landing page components/footer/Footer";
import Hero from "../components/landing page components/hero/Hero";
import OurServices from "../components/landing page components/ourservices/OurServices";

export const LandingPage = () => {
  return (
    <div>
      <Hero />
      <AboutUs id="aboutus" />
      <OurServices id="ourservices" />
      <CallToAction />
      <Footer />
    </div>
  );
};

export default LandingPage;
