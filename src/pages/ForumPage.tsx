import { useContext, useEffect, useState } from "react";
import classes from "./style/ForumPage.module.css";
import AddSectionModal from "../components/forum/AddSectionModal";
import { GetTheJobContext } from "../context/GetTheJobContext";
import PostCard from "../components/forum/PostCard";
import AddPost from "../components/forum/AddPost";
import PostInterface from "../interfaces/PostInterface";
import SectionInterface from "../interfaces/SectionInterface";

export const ForumPage = () => {
  const { sectionId, setSectionId } = useContext(GetTheJobContext);
  const [sections, setSections] = useState<SectionInterface[]>([]);
  const [posts, setPosts] = useState<PostInterface[]>([]);

  useEffect(() => {
    fetch("http://localhost:5000/api/Sections/published")
      .then((response) => response.json())
      .then((data) => {
        setSections(data);
      })
      .catch(() => {
        console.log("error fetching published sections");
      });
  }, [sections]);

  const [showSectionModal, setShowSectionModal] = useState(false);
  const handleSectionModalOpen = () => {
    setShowSectionModal(true);
  };
  const handleSectionModalClose = () => {
    setShowSectionModal(false);
  };

  useEffect(() => {
    fetch(`http://localhost:5000/api/Posts/published/${sectionId}`)
      .then((response) => response.json())
      .then((data) => {
        setPosts(data);
      })
      .catch((error) => {
        console.log("error fetching posts: ", error);
      });
  }, [sectionId]);

  const [showPostModal, setShowPostModal] = useState(false);
  const handlePostModalOpen = () => {
    setShowPostModal(true);
  };
  const handlePostModalClose = () => {
    setShowPostModal(false);
  };

  return (
    <div className={classes.container}>
      <div className={classes.sidebar}>
        {sectionId != null && (
          <div className={classes.actions}>
            <button
              type="button"
              className={classes.addSection}
              onClick={handleSectionModalOpen}
            >
              Add section
            </button>
            <AddSectionModal
              show={showSectionModal}
              handleClose={handleSectionModalClose}
            />
          </div>
        )}
        <ul>
          {sections.map((section) => (
            <li
              key={section.sectionId}
              onClick={() => setSectionId(section.sectionId)}
            >
              {section.sectionName}
            </li>
          ))}
        </ul>
      </div>
      <div className={classes.posts}>
        <div className={classes.postButton}>
          <button
            type="button"
            onClick={handlePostModalOpen}
            className={classes.addPost}
          >
            Add post
          </button>
        </div>
        <AddPost show={showPostModal} handleClose={handlePostModalClose} />
        <div className={classes.list}>
          {posts.map((post) => (
            <PostCard
              postId={post.postId}
              userFirstName={post.userFirstName}
              userLastName={post.userLastName}
              postedDate={post.postedDate}
              postText={post.postText}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default ForumPage;
