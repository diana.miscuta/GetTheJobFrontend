import { useState } from "react";
import PersonalInfo from "../components/resume builder/PersonalInfo";
import { WorkExperience } from "../components/resume builder/WorkExperience";
import classes from "./style/ResumeBuilderPage.module.css";
import Education from "../components/resume builder/Education";
import Skills from "../components/resume builder/Skills";
import Languages from "../components/resume builder/Languages";
import { useNavigate } from "react-router-dom";

export const ResumeBuilderPage = () => {
  const [selectedSection, setSelectedSection] = useState(1);
  const [isSaved, setIsSaved] = useState(false);

  const showSelectedSectionComponent = (selectedSection: number) => {
    switch (selectedSection) {
      case 1:
        return <PersonalInfo setIsSaved={setIsSaved} />;
      case 2:
        return <WorkExperience setIsSaved={setIsSaved} />;
      case 3:
        return <Education setIsSaved={setIsSaved} />;
      case 4:
        return <Skills setIsSaved={setIsSaved} />;
      case 5:
        return <Languages setIsSaved={setIsSaved} />;
    }
  };

  const prevButtonHandler = () => {
    if (
      !isSaved &&
      !window.confirm(
        "Your changes are not saved. Are you sure you want to leave this page?"
      )
    ) {
      return;
    }
    if (selectedSection > 1) {
      setSelectedSection(selectedSection - 1);
    }
    setIsSaved(false);
  };

  const nextButtonHandler = () => {
    if (
      !isSaved &&
      !window.confirm(
        "Your changes are not saved. Are you sure you want to leave this page?"
      )
    ) {
      return;
    }
    if (selectedSection < 5) {
      setSelectedSection(selectedSection + 1);
    }
    setIsSaved(false);
  };

  const navigate = useNavigate();
  const handleBack = () => {
    if (
      !isSaved &&
      !window.confirm(
        "Your changes are not saved. Are you sure you want to leave this page?"
      )
    ) {
      return;
    }
    navigate("/resumes");
  };

  return (
    <div className={classes.container}>
      <div className={classes.info}>
        <div className={classes.selectedSection}>
          {showSelectedSectionComponent(selectedSection)}
        </div>
        <div className={classes.navigate}>
          <button type="button" onClick={prevButtonHandler}>
            Previous
          </button>
          <button type="button" onClick={nextButtonHandler}>
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default ResumeBuilderPage;
