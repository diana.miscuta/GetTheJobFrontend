import { useContext, useEffect, useState } from "react";
import ResumeCard from "../components/resume page/ResumeCard";
import classes from "./style/ResumePage.module.css";
import AddResume from "../components/resume page/AddResume";
import { GetTheJobContext } from "../context/GetTheJobContext";
import { ResumeInterface } from "../interfaces/ResumeInterface";

export const Resume = () => {
  const { userId } = useContext(GetTheJobContext);
  const [resumes, setResumes] = useState<ResumeInterface[]>([]);
  const [showModal, setShowModal] = useState(false);
  const show = () => {
    setShowModal(true);
  };
  const hide = () => {
    setShowModal(false);
  };

  useEffect(() => {
    fetch(`http://localhost:5000/api/Resumes/User/${userId}`)
      .then((response) => response.json())
      .then((data) => {
        setResumes(data);
      })
      .catch(() => {
        console.log("eroare initial fetch");
      });
  }, [resumes, userId]);

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <p>My resumes</p>
        <button type="button" className={classes.addButton} onClick={show}>
          New resume
        </button>
      </div>
      <div className={classes.list}>
        {resumes.map((resume) => (
          <div className={classes.resume}>
            <ResumeCard
              resumeId={resume.resumeId}
              resumeName={resume.resumeName}
              creationDate={resume.creationDate}
            />
          </div>
        ))}
      </div>
      <AddResume show={showModal} handleClose={hide} />
    </div>
  );
};

export default Resume;
