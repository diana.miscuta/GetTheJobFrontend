import { useContext, useEffect, useState } from "react";
import Checklist from "../components/checklist page/checklist/Checklist";
import AddChecklist from "../components/checklist page/modals/AddChecklist";
import classes from "./style/ChecklistPage.module.css";
import { GetTheJobContext } from "../context/GetTheJobContext";
import { ChecklistInterface } from "../interfaces/ChecklistInterface";

export const ChecklistPage = () => {
  const { userId } = useContext(GetTheJobContext);

  const [checklists, setChecklists] = useState<ChecklistInterface[]>([]);
  const [showModal, setShowModal] = useState(false);
  const show = () => {
    setShowModal(true);
  };
  const hide = () => {
    setShowModal(false);
  };

  useEffect(() => {
    fetch(`http://localhost:5000/api/Checklists/User/${userId}`)
      .then((response) => response.json())
      .then((data) => {
        setChecklists(data);
      })
      .catch(() => {
        console.log("error fetching checklists");
      });
  }, [checklists, userId]);

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <p>My checklists</p>
        <button type="button" className={classes.addButton} onClick={show}>
          <p>Add</p>
        </button>
      </div>
      <div className={classes.list}>
        {checklists.map((checklist) => (
          <Checklist
            checklistId={checklist.checklistId}
            title={checklist.checklistName}
          />
        ))}
      </div>
      <AddChecklist show={showModal} handleClose={hide} />
    </div>
  );
};

export default ChecklistPage;
