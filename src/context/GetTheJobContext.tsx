import { createContext } from "react";

export const GetTheJobContext = createContext({
  userId: "",
  setId: (c: string) => {},
  userFirstName: "",
  setFirstName: (c: string) => {},
  userLastName: "",
  setLastName: (c: string) => {},
  userEmail: "",
  setUserEmail: (c: string) => {},
  resumeId: "",
  setResumeId: (c: string) => {},
  sectionId: "",
  setSectionId: (c: string) => {},
});
