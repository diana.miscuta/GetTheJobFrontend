export interface EventInterface {
  eventId: string;
  eventName: string;
  eventStartDate: Date;
  eventStopDate: Date;
  eventLocation: string;
  eventDetails: string;
  eventColor: number;
}

export default EventInterface;
