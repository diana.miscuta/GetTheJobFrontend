export interface PostInterface {
  postId: string;
  userFirstName: string;
  userLastName: string;
  postedDate: string;
  postText: string;
}

export default PostInterface;
