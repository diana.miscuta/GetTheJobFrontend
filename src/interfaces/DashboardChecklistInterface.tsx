export interface DashboardChecklistInterface {
  checklistId: string;
  checklistName: string;
  checklistStatus: number;
}

export default DashboardChecklistInterface;
