export interface ChecklistItemInterface {
  itemId: string;
  text: string;
  status: boolean;
}

export default ChecklistItemInterface;
