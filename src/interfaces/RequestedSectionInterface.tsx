export interface RequestedSectionInterface {
  sectionId: string;
  sectionName: string;
  userFirstName: string;
  userLastName: string;
}

export default RequestedSectionInterface;
