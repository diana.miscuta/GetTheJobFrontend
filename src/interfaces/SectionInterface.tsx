export interface SectionInterface {
  sectionId: string;
  sectionName: string;
}

export default SectionInterface;
