export interface ResumeInterface {
  resumeId: string;
  resumeName: string;
  creationDate: string;
}
