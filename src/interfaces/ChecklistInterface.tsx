export interface ChecklistInterface {
  checklistId: string;
  checklistName: string;
}

export default ChecklistInterface;
