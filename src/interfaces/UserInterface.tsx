export interface UserInterface {
  userId: string;
  userEmail: string;
  userFirstName: string;
  userLastName: string;
  userGoal1: string;
  userGoal2: string;
  userGoal3: string;
}

export default UserInterface;
